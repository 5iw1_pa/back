#!/bin/bash
docker build \
  --target app_php \
  -t $TARGETED_REGISTRY_IMAGE_PHP \
  .
docker build \
  --target app_nginx \
  -t $TARGETED_REGISTRY_IMAGE_SRV \
  .

docker push $TARGETED_REGISTRY_IMAGE_SRV
docker push $TARGETED_REGISTRY_IMAGE_PHP
