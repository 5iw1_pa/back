<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220626221751 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE mission_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "profile_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ticket_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE message (id INT NOT NULL, ticket_id_id INT DEFAULT NULL, user_id_id INT NOT NULL, date_message DATE NOT NULL, description VARCHAR(1000) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6BD307F5774FDDC ON message (ticket_id_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F9D86650F ON message (user_id_id)');
        $this->addSql('CREATE TABLE mission (id INT NOT NULL, id_author_id INT NOT NULL, id_candidate_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL, publication_date DATE NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, documents TEXT DEFAULT NULL, skills TEXT DEFAULT NULL, status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9067F23C76404F3C ON mission (id_author_id)');
        $this->addSql('CREATE INDEX IDX_9067F23CB27CF2F3 ON mission (id_candidate_id)');
        $this->addSql('COMMENT ON COLUMN mission.documents IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN mission.skills IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE mission_user (mission_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(mission_id, user_id))');
        $this->addSql('CREATE INDEX IDX_A4D17A46BE6CAE90 ON mission_user (mission_id)');
        $this->addSql('CREATE INDEX IDX_A4D17A46A76ED395 ON mission_user (user_id)');
        $this->addSql('CREATE TABLE "profile" (id INT NOT NULL, status INT NOT NULL, name VARCHAR(255) NOT NULL, siret VARCHAR(14) NOT NULL, phone_number VARCHAR(10) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, departement VARCHAR(255) DEFAULT NULL, town VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, certified BOOLEAN NOT NULL, documents TEXT DEFAULT NULL, skills TEXT DEFAULT NULL, description VARCHAR(1000) DEFAULT NULL, price INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN "profile".documents IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN "profile".skills IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE ticket (id INT NOT NULL, user_id_id INT NOT NULL, title VARCHAR(1000) NOT NULL, status INT NOT NULL, date_ticket DATE NOT NULL, description VARCHAR(1000) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_97A0ADA39D86650F ON ticket (user_id_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, profile_id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, token VARCHAR(255) DEFAULT NULL, picture TEXT DEFAULT NULL, premium_until TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, date_token TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE INDEX IDX_8D93D649CCFA12B8 ON "user" (profile_id)');
        $this->addSql('COMMENT ON COLUMN "user".picture IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F5774FDDC FOREIGN KEY (ticket_id_id) REFERENCES ticket (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F9D86650F FOREIGN KEY (user_id_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23C76404F3C FOREIGN KEY (id_author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23CB27CF2F3 FOREIGN KEY (id_candidate_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mission_user ADD CONSTRAINT FK_A4D17A46BE6CAE90 FOREIGN KEY (mission_id) REFERENCES mission (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mission_user ADD CONSTRAINT FK_A4D17A46A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA39D86650F FOREIGN KEY (user_id_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D649CCFA12B8 FOREIGN KEY (profile_id) REFERENCES "profile" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE mission_user DROP CONSTRAINT FK_A4D17A46BE6CAE90');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D649CCFA12B8');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F5774FDDC');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F9D86650F');
        $this->addSql('ALTER TABLE mission DROP CONSTRAINT FK_9067F23C76404F3C');
        $this->addSql('ALTER TABLE mission DROP CONSTRAINT FK_9067F23CB27CF2F3');
        $this->addSql('ALTER TABLE mission_user DROP CONSTRAINT FK_A4D17A46A76ED395');
        $this->addSql('ALTER TABLE ticket DROP CONSTRAINT FK_97A0ADA39D86650F');
        $this->addSql('DROP SEQUENCE message_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE mission_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "profile_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE ticket_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE mission');
        $this->addSql('DROP TABLE mission_user');
        $this->addSql('DROP TABLE "profile"');
        $this->addSql('DROP TABLE ticket');
        $this->addSql('DROP TABLE "user"');
    }
}
