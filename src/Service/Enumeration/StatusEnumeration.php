<?php
namespace App\Service\Enumeration;

class StatusEnumeration
{
    const COMING = 0;
    const IN_PROGRESS = 1;
    const COMPLETED = 2;
    const OPEN = 3;
    const CLOSE = 4;
}