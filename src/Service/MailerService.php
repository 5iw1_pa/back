<?php


namespace App\Service;


use App\Entity\Mission;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailerService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendRegisterConfirmedMail(User $user, string $url)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Validation de votre compte')
            ->htmlTemplate('email/register_confirmed.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'url'=>$url.'?email='.$user->getEmail().'&token='.$user->getToken()
            ])

        ;
        $this->mailer->send($email);
    }

    public function sendConfirmedMail(User $user, string $url)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Validation de votre email')
            ->htmlTemplate('email/confirm_email.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'url'=>$url.'?email='.$user->getEmail().'&token='.$user->getToken()
            ])

        ;
        $this->mailer->send($email);
    }

    public function sendAddUserMail(User $user, string $url)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Création de votre compte')
            ->htmlTemplate('email/add_user.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'enterprise'=> $user->getProfile()->getName(),
                'url'=>$url.'?email='.$user->getEmail().'&token='.$user->getToken()
            ])
        ;
        $this->mailer->send($email);
    }

    public function sendForgotPasswordMail(User $user, string $url)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Mot de passe oublié')
            ->htmlTemplate('email/forgot_password.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'url'=>$url.'?email='.$user->getEmail().'&token='.$user->getToken()
            ])
        ;
        $this->mailer->send($email);
    }

    public function sendChoiceUser(User $user, Mission $mission, string $url)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Vous avez été choisi')
            ->htmlTemplate('email/choice_user.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'enterprise'=> $mission->getIdAuthor()->getProfile()->getName(),
                'mission'=> $mission->getTitle(),
                'url'=>$url
            ])
        ;
        $this->mailer->send($email);
    }

    public function sendRemoveChoiceUser(User $user, Mission $mission)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Vous n\'etes plus affecter')
            ->htmlTemplate('email/remove_choice_user.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'enterprise'=> $mission->getIdAuthor()->getProfile()->getName(),
                'mission'=> $mission->getTitle()
            ])
        ;
        $this->mailer->send($email);
    }
    public function sendAbandonMission(Mission $mission, $url)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($mission->getIdAuthor()->getEmail())
            ->subject('Vous n\'avez plus de candidat')
            ->htmlTemplate('email/abandon_mission.html.twig')
            ->context([
                'name'=>$mission->getIdAuthor()->getFirstname()." ".$mission->getIdAuthor()->getLastname(),
                'enterprise'=> $mission->getIdAuthor()->getProfile()->getName(),
                'mission'=> $mission->getTitle(),
                'url'=>$url
            ])
        ;
        $this->mailer->send($email);
    }

    public function sendNotifyUser(User $user, Mission $mission,$url)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Votre profil a été repéré')
            ->htmlTemplate('email/notify_user.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'enterprise'=> $mission->getIdAuthor()->getProfile()->getName(),
                'mission'=> $mission->getTitle(),
                'url'=> $url
            ])
        ;
        $this->mailer->send($email);
    }

    public function sendChangeEmail(User $user, string $url, string $newEmail)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Changement d\'email')
            ->htmlTemplate('email/change_email_user.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'newEmail' => $newEmail,
                'url'=>$url.'?email='.$user->getEmail().'&token='.$user->getToken().'&newEmail='.$newEmail
            ])
        ;
        $this->mailer->send($email);
    }

    public function sendCertifiedMail(User $user)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@darkmalt.com')
            ->to($user->getEmail())
            ->subject('Vous avez été certifié')
            ->htmlTemplate('email/certified_profile.html.twig')
            ->context([
                'name'=>$user->getFirstname()." ".$user->getLastname(),
                'enterprise'=>$user->getProfile()->getName()
            ])
        ;
        $this->mailer->send($email);
    }
}