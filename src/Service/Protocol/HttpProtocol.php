<?php

namespace App\Service\Protocol;

use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\Response;

class HttpProtocol
{
  public function __construct()
  {
  }

  public function get($url, $client, $query, $authorization): Response
  {
    try {
      $headers = ["headers" => ['Content-Type' => 'application/json']];
      if ($authorization !== null) {
        $headers['auth_bearer'] = $authorization;
      }
      $query = $this->makeQuery($query);
      $response = $client->request("GET", $url . $query, $headers);
      return new Response($response->getContent(), $response->getStatusCode(),["content-type"=>"application/json"]);
    } catch (Exception $e) {
      return new Response(json_encode($e), $response->getStatusCode(),["content-type"=>"application/json"]);
    }
  }

  public function post($url, $client, $body, $authorization): Response
  {
    try {
      $headers = [
        "headers" => ['Content-Type' => 'application/json'],
        "body" => $body,
      ];
      if ($authorization !== null) {
        $headers['auth_bearer'] = $authorization;
      }
      $response = $client->request("POST", $url, $headers);
      return new Response($response->getContent(), $response->getStatusCode(),["content-type"=>"application/json"]);
    } catch (Exception $e) {
      return new Response(json_encode($e), $response->getStatusCode(),["content-type"=>"application/json"]);
    }
  }

  public function put($url, $client, $body, $authorization): Response
  {
    try {
      $headers = [
        "headers" => ['Content-Type' => 'application/json'],
        "body" => $body,
      ];
      if ($authorization !== null) {
        $headers['auth_bearer'] = $authorization;
      }
      $response = $client->request("PUT", $url, $headers);
      return new Response($response->getContent(), $response->getStatusCode(),["content-type"=>"application/json"]);
    } catch (Exception $e) {
      return new Response(json_encode($e), $response->getStatusCode(),["content-type"=>"application/json"]);
    }
  }
  
  private function makeQuery($query) 
  {
    if ($query !== null) {
      $query = $query->all();
      $output = implode('&', array_map(
        function ($v, $k) {
            if(is_array($v)){
                return $k.'[]='.implode('&'.$k.'[]=', $v);
            }else{
                return $k.'='.$v;
            }
        }, 
        $query, 
        array_keys($query)
      ));
      return "?$output";
    } else {
      return "";
    }
  }
}
