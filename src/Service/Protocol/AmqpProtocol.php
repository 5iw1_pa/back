<?php

namespace App\Service\Protocol;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class AmqpProtocol
{
  private $connection;
  public function __construct()
  {
    if (!$this->connection) {
      $this->connection = $this->connection();
    }
  }

  private function connection()
  {
    return new AMQPStreamConnection($_SERVER["RABBITMQ_HOST"], $_SERVER["RABBITMQ_PORT"], $_SERVER["RABBITMQ_USER"], $_SERVER["RABBITMQ_PASSWORD"]);
  }

  public function send($queue, $exchange, $msg)
  {
    $channel = $this->connection->channel();
    $channel->queue_declare($queue, false, false, false, false);
    $channel->exchange_declare($exchange, 'direct', false, true, false);
    $channel->queue_bind($queue, $exchange);

    $message = new AMQPMessage($msg, array('content_type' => 'text/plain', 'delivery_mode' => 2));
    $channel->basic_publish($message, $exchange);

    $channel->close();
  }
}
