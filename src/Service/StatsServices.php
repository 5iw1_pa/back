<?php


namespace App\Service;


use App\Entity\User;
use App\Repository\MissionRepository;
use App\Repository\ProfileRepository;
use App\Repository\TicketRepository;
use App\Service\Enumeration\StatusEnumeration;
use App\Service\Protocol\HttpProtocol;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class StatsServices
{
    private $profileRepository;
    private $missionRepository;
    private $ticketRepository;
    private $httpProtocol;
    private $client;
    private $security;
    public function __construct(ProfileRepository $profileRepository, MissionRepository $missionRepository,HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security, TicketRepository $ticketRepository)
    {
        $this->profileRepository = $profileRepository;
        $this->missionRepository = $missionRepository;
        $this->ticketRepository = $ticketRepository;
        $this->httpProtocol = $httpProtocol;
        $this->client = $client;
        $this->security = $security;
    }

    public function getFreelanceStats(User $user)
    {
        $paymentResponse = $this->httpProtocol->get("http://payment_api:80/payment/user", $this->client, null, $this->security->getToken()->getCredentials());
        $payments = json_decode($paymentResponse->getContent(),true);
        $refundResponse =  $this->httpProtocol->get("http://payment_api:80/refund/profile/refunded", $this->client, null, $this->security->getToken()->getCredentials());
        $refunds = json_decode($refundResponse->getContent(),true);
        $amount = 0;
        foreach ($payments as $payment)
        {
            $amount += $payment["amount"];
        }
        foreach ($refunds as $refund)
        {
            $amount -= $refund["payment_id"]["amount"];
        }
        return[
            "stats_freelance_participe" => count($user->getMissionsParticipated()),
            "stats_freelance_candidate" => count($user->getCandidateMission()),
            "stats_freelance_payment" => $amount
        ];
    }

    public function getEnterpriseStats(User $user)
    {
        return[
            "stats_enterprise_missions_create" => count($user->getMissionsCreated()),
            "stats_enterprise_missions_coming" => count($this->missionRepository->findBy(["status" =>StatusEnumeration::COMING, "idAuthor" => $user->getId()])),
            "stats_enterprise_missions_progress" => count($this->missionRepository->findBy(["status" => StatusEnumeration::IN_PROGRESS, "idAuthor" => $user->getId()])),
            "stats_enterprise_missions_completed" => count($this->missionRepository->findBy(["status" => StatusEnumeration::COMPLETED, "idAuthor" => $user->getId()]))
        ];
    }

    public function getEnterpriseAdminStats(User $user)
    {
        $paymentResponse = $this->httpProtocol->get("http://payment_api:80/payment/profile", $this->client, null, $this->security->getToken()->getCredentials());
        $payments = json_decode($paymentResponse->getContent(),true);
        $refundResponse =  $this->httpProtocol->get("http://payment_api:80/refund/profile/refunded", $this->client, null, $this->security->getToken()->getCredentials());
        $refunds = json_decode($refundResponse->getContent(),true);
        $statsEnterpriseMissionsCreate = 0;
        $statsEnterpriseMissionsComing = 0;
        $statsEnterpriseMissionsProgress = 0;
        $statsEnterpriseMissionsCompleted = 0;
        $amount = 0;
        foreach ($payments as $payment)
        {
            $amount += $payment["amount"];
        }
        foreach ($refunds as $refund)
        {
            $amount -= $refund["payment_id"]["amount"];
        }
        foreach ($user->getProfile()->getUsers() as $userProfile)
        {
            $statsEnterpriseMissionsCreate += count($this->missionRepository->findBy(["idAuthor" => $userProfile->getId()]));
            $statsEnterpriseMissionsComing += count($this->missionRepository->findBy(["status" =>StatusEnumeration::COMING, "idAuthor" => $userProfile->getId()]));
            $statsEnterpriseMissionsProgress += count($this->missionRepository->findBy(["status" => StatusEnumeration::IN_PROGRESS, "idAuthor" => $userProfile->getId()]));
            $statsEnterpriseMissionsCompleted += count($this->missionRepository->findBy(["status" => StatusEnumeration::COMPLETED, "idAuthor" => $userProfile->getId()]));
        }
        return[
            "stats_enterprise_admin_missions_create" => $statsEnterpriseMissionsCreate,
            "stats_enterprise_admin_missions_coming" => $statsEnterpriseMissionsComing,
            "stats_enterprise_admin_missions_progress" => $statsEnterpriseMissionsProgress,
            "stats_enterprise_admin_missions_completed" => $statsEnterpriseMissionsCompleted,
            "stats_enterprise_payment" => $amount,
            "stats_enterprise_admin_users" => count($user->getProfile()->getUsers())
        ];
    }

    public function getAdminStats()
    {
        return[
            "stats_admin_users" => count($this->profileRepository->findAll()),
            "stats_admin_enterprises" => count($this->profileRepository->findBy(["status" => 1])),
            "stats_admin_freelances" => count($this->profileRepository->findBy(["status" => 2])),
            "stats_admin_uncertified" => count($this->profileRepository->findBy(["certified" => false])),
            "stats_admin_tickets" => count($this->ticketRepository->findBy(["status" => StatusEnumeration::OPEN])),
        ];
    }
}