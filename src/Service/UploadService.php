<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Flex\Configurator\ContainerConfigurator;

class UploadService
{
  private $targetDirectory;
  private $slugger;

  public function __construct(string $targetDirectory, SluggerInterface $slugger)
  {
    $this->targetDirectory = $targetDirectory;

    $this->slugger = $slugger;
  }

  public function checkUpload($documents)
  {
      $documentsList = [];

      foreach ($documents as $document) {
          if (gettype($document) === "string" && $this->get($document) !== null) {
              $documentsList[] = $document;
          } else if (gettype($document) === "array") {
              $doc = $this->upload($document);
              if (!$doc) continue;
              $documentsList[] = $doc;
          }
      }

      return $documentsList;
  }

  public function upload(array $file, string $targetDirectory = "")
  {
    $this->targetDirectory .= $targetDirectory;
    $fileName = uniqid() . "_" . $file["name"];
    // Check if document is PDF, PNG or JPEG
    if (str_starts_with($file["value"], "data:application/pdf") ||
        str_starts_with($file["value"], "data:image/png") || 
        str_starts_with($file["value"], "data:image/jpeg")) {
      try {
        file_put_contents($this->targetDirectory . '/' . $fileName, $file["value"]);
      } catch (FileException $e) {
        throw new \Exception($e);
      }
      return $fileName;
    } else {
      return false;
    }
  }

  public function remove(string $fileName, string $targetDirectory = "")
  {
    $this->targetDirectory .= $targetDirectory;
    $file = $fileName;
    if (file_exists($this->targetDirectory . "/" . $file)) {
      unlink($this->targetDirectory . "/" . $file);
    }
  }

  public function get(string $fileName, string $targetDirectory = ""): ?string
  {
    $this->targetDirectory .= $targetDirectory;
    $file = $fileName;
    if (file_exists($this->targetDirectory . "/" . $file)) {
      return file_get_contents($this->targetDirectory . "/" . $file);
    }
    return null;
  }
}
