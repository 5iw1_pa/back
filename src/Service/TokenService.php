<?php


namespace App\Service;


use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class TokenService
{
    private $serializer;
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function checkToken(?User $user, string $token) : Response
    {

        if(empty($user) || $user->getToken() != $token)
        {

            return new Response($this->serializer->serialize("Le lien n est pas bon", 'json'), 400, ["content-type"=>"application/json"]);

        }
        elseif(empty($user->getDateToken()) || date_add($user->getDateToken(),date_interval_create_from_date_string('900 seconds' )) < new \DateTime('NOW'))
        {

            return new Response($this->serializer->serialize("Le lien est expire", 'json'), 400, ["content-type"=>"application/json"]);

        }
        return new Response($this->serializer->serialize("Token valide", 'json'), 200, ["content-type"=>"application/json"]);

    }
}