<?php

namespace App\Entity;

use App\Repository\ProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProfileRepository::class)
 * @ORM\Table(name="`profile`")
 * @UniqueEntity("siret",message="Ce siret est deja utilise")
 */
class Profile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get_profile","get_user"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"create_user","get_profile","get_user", "user_search_api"})
     * @Assert\Range(
     *      min = 1,
     *      max = 2,
     *      notInRangeMessage = "Le choix doit être {{ min }} ou {{ max }}",
     * )
     * @Assert\NotBlank(message="Le status ne peut pas être vide")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"create_search_user","create_user","get_user","get_profile", "update_profile", "user_search_api", "mission_search_api"})
     * @Assert\Length(
     *     max = 255,
     *     maxMessage="le nom de l'entreprise est trop long",
     *     groups="update_profile"
     * )
     * @Assert\NotBlank(message="Le nom de l'entreprise ne peut pas être vide")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=14, unique=true)
     * @Groups({"create_search_user","create_user","get_user","get_profile", "user_search_api"})
     * @Assert\Length(
     *     min = 14,
     *     max = 14,
     *     maxMessage="Le siret doit faire 14 chiffres",
     *     minMessage="Le siret doit faire 14 chiffres",
     *     exactMessage="Le siret doit faire 14 chiffres"
     * )
     * @Assert\NotBlank(message="Le siret ne peut pas être vide")
     * @Assert\Regex("#^[0-9]+$#u", message="Le siret ne peux contenir que des chiffres")
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({"create_user","get_user","get_profile", "update_profile"})
     * @Assert\Regex("#^(?:\d{10}|)$#u", message="Le numero de téléphone ne peux contenir que des chiffres",
     *     groups="update_profile")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"create_user","get_user","get_profile", "update_profile"})
     * @Assert\Length(
     *     max = 255,
     *     maxMessage="L'adresse est trop longue",
     *     groups="update_profile"
     * )
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"create_user","get_user","get_profile", "update_profile"})
     * @Assert\Length(
     *     max = 10,
     *     maxMessage="Le département est trop long",
     *     groups="update_profile"
     * )
     */
    private $departement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"create_user","get_user","get_profile", "update_profile"})
     * @Assert\Length(
     *     max = 255,
     *     maxMessage="La ville est trop longue",
     *     groups="update_profile"
     * )
     */
    private $town;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"create_user","get_user","get_profile", "update_profile"})
     * @Assert\Length(
     *     max = 255,
     *     maxMessage="Le pays est trop long",
     *     groups="update_profile"
     * )
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="profile")
     * @Groups({"get_profile"})
     */
    private $users;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"get_user", "get_profile", "user_search_api"})
     */
    private $certified;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"get_documents","update_profile"})
     */
    private $documents = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"get_user", "get_profile", "update_profile", "user_search_api"})
     */
    private $skills = [];

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @Groups({"get_user", "get_profile", "update_profile"})
     * @Assert\Length(
     *     max = 1000,
     *     maxMessage="La description est trop longue",
     *     groups="update_profile"
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get_user", "get_profile", "update_profile"})
     * @Assert\Type(
     *     type="integer",
     *     message="le tarif doit etre un nombre",
     *     groups="update_profile"
     * )
     * @Assert\Range(
     *     min = "0",
     *     minMessage = "le tarif ne peut pas être en dessous de 0",
     *     groups="update_profile"
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get_user","update_user_admin", "user_search_api", "get_profile", "mission_search_api"})
     */
    private $premium_until;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get_user"})
     */
    private $premiumTry;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = trim($name);

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = str_replace(' ','',$siret);

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = str_replace(' ','',$phoneNumber);

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = trim($address);

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): self
    {
        $this->departement = trim($departement);

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = trim($town);

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = trim($country);

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setProfile($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getProfile() === $this) {
                $user->setProfile(null);
            }
        }

        return $this;
    }

    public function getCertified(): ?bool
    {
        return $this->certified;
    }

    public function setCertified(bool $certified): self
    {
        $this->certified = $certified;

        return $this;
    }

    public function getDocuments(): ?array
    {
        return $this->documents;
    }

    public function setDocuments(?array $documents): self
    {
        $this->documents = $documents;

        return $this;
    }

    public function getSkills(): ?array
    {
        return $this->skills;
    }

    public function setSkills(?array $skills): self
    {
        $this->skills = $skills;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = trim($description);

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPremiumUntil(): ?\DateTimeInterface
    {
        return $this->premium_until;
    }

    public function setPremiumUntil(?\DateTimeInterface $premium_until): self
    {
        $this->premium_until = $premium_until;

        return $this;
    }

    public function getPremiumTry(): ?bool
    {
        return $this->premiumTry;
    }

    public function setPremiumTry(?bool $premiumTry): self
    {
        $this->premiumTry = $premiumTry;

        return $this;
    }
}
