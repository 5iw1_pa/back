<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity("email",message="Cette email est deja utilise", groups={"add_user","update_email"})
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"create_search_user","get_user","get_mission","get_profile", "get_message", "get_ticket", "user_search_api"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"create_search_user","create_user","get_user","get_mission","get_profile","email_user","add_user","update_user_admin", "get_message", "get_ticket", "user_search_api","update_email"})
     * @Assert\Email(message="l'email n'est pas valide", groups={"add_user","update_email"})
     * @Assert\NotBlank(groups={"add_user","update_email"}, message="l'email ne peut pas etre vide")
     * @Assert\Length(
     *     max = 255,
     *     groups={"add_user","update_email"},
     *     maxMessage="l'email est trop long"
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @OA\Property(type="array", @OA\Items(type="string"))
     * @Groups({"get_role","update_user_admin"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"create_user", "password_user",})
     * @Assert\Regex("#(?=.*[a-z])#", message="Il manque au moins une minuscule",groups={"password_user"})
     * @Assert\Regex("#(?=.*[A-Z])#", message="Il manque au moins une majuscule",groups={"password_user"})
     * @Assert\Regex("#(?=.*[0-9])#", message="Il manque au moins un chiffre",groups={"password_user"})
     * @Assert\Regex("#(?=.*\W)#", message="Il manque au moins un caractère spécial",groups={"password_user"})
     * @Assert\Length(
     *     groups={"password_user"},
     *     min = 7,
     *     max = 45,
     *     minMessage = "Votre mot de passe est trop court, il doit avoir au minimum {{ limit }} caractères.",
     *     maxMessage = "Votre mot de passe est trop long, il doit avoir au maximum {{ limit }} caractères."
     * )
     * @Assert\NotBlank(groups="password_user")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"create_search_user","create_user","get_mission","get_profile", "get_user", "update_user","add_user", "get_message", "get_ticket", "user_search_api","update_user_admin"})
     * @Assert\NotBlank(groups={"update_user","add_user"}, message="Le prenom ne peut pas etre vide")
     * @Assert\Regex("#^[\p{Latin}' -]+$#u", message="Le prenom ne peut pas contenir de chiffre ou de caractères spécial",groups={"update_user","add_user"})
     * @Assert\Length(
     *     max = 255,
     *     groups={"update_user","add_user","update_user_admin"},
     *     maxMessage="Le prenom est trop long"
     * )
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"create_search_user","create_user","get_user","get_mission","get_profile", "update_user","add_user", "get_message", "get_ticket", "user_search_api","update_user_admin"})
     * @Assert\NotBlank(groups={"update_user","add_user"}, message="Le nom ne peut pas etre vide")
     * @Assert\Regex("#^[\p{Latin}' -]+$#u", message="Le nom ne peut pas contenir de chiffre ou de caractères spécial",groups={"update_user","add_user"})
     * @Assert\Length(
     *     max = 255,
     *     groups={"update_user","add_user","update_user_admin"},
     *     maxMessage="Le nom est trop long"
     * )
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     * @OA\Property(type="array", @OA\Items(type="string"))
     * @Groups({"get_user","User", "user_search_api", "mission_search_api"})
     */
    private $profile;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"add_picture_user","get_user", "update_user"})
     */
    private $picture = [];

    /**
     * @ORM\OneToMany(targetEntity=Mission::class, mappedBy="idAuthor", orphanRemoval=true)
     */
    private $missionsCreated;

    /**
     * @ORM\OneToMany(targetEntity=Mission::class, mappedBy="idCandidate")
     */
    private $missionsParticipated;

    /**
     * @ORM\ManyToMany(targetEntity=Mission::class, mappedBy="candidate")
     */
    private $candidateMission;

    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateToken;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="user_id", orphanRemoval=true)
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="user_id", orphanRemoval=true)
     */
    private $tickets;


    public function __construct()
    {
        $this->missionsCreated = new ArrayCollection();
        $this->missionsParticipated = new ArrayCollection();
        $this->candidateMission = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->tickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRoles(string $roles): self
    {
        $this->roles[] = $roles;
        $this->roles = array_unique($this->roles);

        return $this;

    }

    public function removeRoles(string $roles): self
    {
        $key = array_search($roles,$this->roles);
        if($key !== false) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = trim($firstname);

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = trim($lastname);

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getPicture(): ?array
    {
        return $this->picture;
    }

    public function setPicture(?array $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return Collection<int, Mission>
     */
    public function getMissionsCreated(): Collection
    {
        return $this->missionsCreated;
    }

    public function addMissionsCreated(Mission $missionsCreated): self
    {
        if (!$this->missionsCreated->contains($missionsCreated)) {
            $this->missionsCreated[] = $missionsCreated;
            $missionsCreated->setIdAuthor($this);
        }

        return $this;
    }

    public function removeMissionsCreated(Mission $missionsCreated): self
    {
        if ($this->missionsCreated->removeElement($missionsCreated)) {
            // set the owning side to null (unless already changed)
            if ($missionsCreated->getIdAuthor() === $this) {
                $missionsCreated->setIdAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Mission>
     */
    public function getMissionsParticipated(): Collection
    {
        return $this->missionsParticipated;
    }

    public function addMissionsParticipated(Mission $missionsParticipated): self
    {
        if (!$this->missionsParticipated->contains($missionsParticipated)) {
            $this->missionsParticipated[] = $missionsParticipated;
            $missionsParticipated->setIdCandidate($this);
        }

        return $this;
    }

    public function removeMissionsParticipated(Mission $missionsParticipated): self
    {
        if ($this->missionsParticipated->removeElement($missionsParticipated)) {
            // set the owning side to null (unless already changed)
            if ($missionsParticipated->getIdCandidate() === $this) {
                $missionsParticipated->setIdCandidate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Mission>
     */
    public function getCandidateMission(): Collection
    {
        return $this->candidateMission;
    }


    public function addCandidateMission(Mission $candidateMission): self
    {
        if (!$this->candidateMission->contains($candidateMission)) {
            $this->candidateMission[] = $candidateMission;
            $candidateMission->addCandidate($this);
        }

        return $this;
    }

    public function removeCandidateMission(Mission $candidateMission): self
    {
        if ($this->candidateMission->removeElement($candidateMission)) {
            $candidateMission->removeCandidate($this);
        }

        return $this;
    }

    public function getDateToken(): ?\DateTimeInterface
    {
        return $this->dateToken;
    }

    public function setDateToken(?\DateTimeInterface $dateToken): self
    {
        $this->dateToken = $dateToken;

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUserId($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getUserId() === $this) {
                $message->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setUserId($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getUserId() === $this) {
                $ticket->setUserId(null);
            }
        }

        return $this;
    }
}