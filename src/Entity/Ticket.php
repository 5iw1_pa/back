<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\TicketRepository;
use OpenApi\Annotations as OA;



/**
 * @ORM\Entity(repositoryClass=TicketRepository::class)
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get_ticket"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Groups({"get_ticket", "create_ticket", "get_message"})
     * @Assert\Length(
     *     max = 1000,
     *     maxMessage="Le titre du ticket est trop long"
     * )
     * @Assert\NotBlank(message="Le titre ne peut pas être vide")
     */
    private $title;

    /**
     * @ORM\Column(type="integer")s
     * @Groups({"get_ticket", "create_ticket"})
     */
    private $status;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get_ticket"})
     */
    private $dateTicket;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="ticket_id", orphanRemoval=true)
     * * @Groups({"get_ticket"})
     */
    private $messages;

    /**
     * @ORM\ManyToOne(targetEntity=user::class, inversedBy="tickets")
     * @Groups({"get_ticket"})
     */
    private $user_id;

    /**
     * 
     * @ORM\Column(type="string", length=1000)
     * @Groups({"get_ticket", "create_ticket"})
     * @Assert\Length(
     *     max = 1000,
     *     maxMessage="Le message est trop long"
     * )
     * @Assert\NotBlank(message="La description ne peut pas être vide")
     */
    private $description;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }

    public function setId(int $id): self
    {
        $this->$id = $id;

        return $this;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = trim($title);

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateTicket(): ?\DateTimeInterface
    {
        return $this->dateTicket;
    }

    public function setDateTicket(\DateTimeInterface $dateTicket): self
    {
        $this->dateTicket = $dateTicket;

        return $this;
    }

    /**
     * @return Collection<int, Message>
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setTicketId($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getTicketId() === $this) {
                $message->setTicketId(null);
            }
        }

        return $this;
    }

    public function getUserId(): ?user
    {
        return $this->user_id;
    }

    public function setUserId(?user $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
