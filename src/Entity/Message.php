<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get_message", "get_ticket"})
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get_message", "get_ticket"})
     */
    private $dateMessage;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Groups({"get_message", "create_message", "get_ticket"})
     * @Assert\Length(
     *     max = 1000,
     *     maxMessage="Le message est trop long"
     * )
     * @Assert\NotBlank(message="La description ne peut pas être vide")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=ticket::class, inversedBy="messages")
     * @Groups({"get_message"})
     */
    private $ticket_id;

    /**
     * @ORM\ManyToOne(targetEntity=user::class, inversedBy="messages")
     * @Groups({"get_message", "get_ticket"})
     */
    private $user_id;

    public function setId(int $id): self
    {
        $this->$id = $id;

        return $this;
    }
    public function getId(): ?int
    {
        return $this->id;
    }


    public function getDateMessage(): ?\DateTimeInterface
    {
        return $this->dateMessage;
    }

    public function setDateMessage(\DateTimeInterface $dateMessage): self
    {
        $this->dateMessage = $dateMessage;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = trim($description);

        return $this;
    }

    public function getTicketId(): ?Ticket
    {
        return $this->ticket_id;
    }

    public function setTicketId(?Ticket $ticket_id): self
    {
        $this->ticket_id = $ticket_id;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }
}