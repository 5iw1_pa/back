<?php

namespace App\Entity;

use App\Repository\MissionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MissionRepository::class)
 */
class Mission
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get_mission", "mission_search_api"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"create_mission","get_mission", "mission_search_api"})
     * @Assert\Length(
     *     max = 255,
     *     maxMessage="le nom de la mission est trop long"
     * )
     * @Assert\NotBlank(message="Le nom de la mission ne peut pas être vide")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"create_mission","get_mission", "mission_search_api"})
     * @Assert\Length(
     *     max = 5000,
     *     maxMessage="la description est trop longue"
     * )
     * @Assert\NotBlank(message="La description ne peut pas être vide")
     */
    private $description;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get_mission"})
     */
    private $publicationDate;

    /**
     * @ORM\Column(type="date")
     * @Groups({"create_mission","get_mission", "mission_search_api"})
     * @Assert\NotBlank(message="La date de début ne peut pas être vide")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     * @Groups({"create_mission","get_mission", "mission_search_api"})
     * @Assert\NotBlank(message="La date de fin ne peut pas être vide")
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="missionsCreated")
     * @Groups({"get_mission", "mission_search_api"})
     */
    private $idAuthor;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="missionsParticipated")
     * @Groups({"get_mission"})
     */
    private $idCandidate;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"get_mission", "create_mission"})
     */
    private $documents = [];

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="candidateMission")
     * @Groups({"create_mission","get_mission"})
     */
    private $candidate;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"create_mission","get_mission", "mission_search_api"})
     */
    private $skills = [];

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get_mission", "mission_search_api"})
     */
    private $status;

    public function __construct()
    {
        $this->candidate = new ArrayCollection();
    }
    public function setId(int $id): self
    {
        $this->$id = $id;

        return $this;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = trim($title);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = trim($description);

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getIdAuthor(): ?User
    {
        return $this->idAuthor;
    }

    public function setIdAuthor(?User $idAuthor): self
    {
        $this->idAuthor = $idAuthor;

        return $this;
    }

    public function getIdCandidate(): ?User
    {
        return $this->idCandidate;
    }

    public function setIdCandidate(?User $idCandidate): self
    {
        $this->idCandidate = $idCandidate;

        return $this;
    }

    public function getDocuments(): ?array
    {
        return $this->documents;
    }

    public function setDocuments(?array $documents): self
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getCandidate(): Collection
    {
        return $this->candidate;
    }

    public function setCandidate(?array $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function addCandidate(User $candidate): self
    {
        if (!$this->candidate->contains($candidate)) {
            $this->candidate[] = $candidate;
        }

        return $this;
    }

    public function removeCandidate(User $candidate): self
    {
        $this->candidate->removeElement($candidate);

        return $this;
    }

    public function getSkills(): ?array
    {
        return $this->skills;
    }

    public function setSkills(?array $skills): self
    {
        $this->skills = $skills;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
