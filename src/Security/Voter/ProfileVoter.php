<?php

namespace App\Security\Voter;


use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ProfileVoter extends Voter
{
    public const UPDATE_PROFILE = 'UPDATE_PROFILE';

    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::UPDATE_PROFILE])
            && $subject instanceof \App\Entity\Profile;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::UPDATE_PROFILE:
                if($this->security->isGranted("ROLE_FREELANCE"))
                    return true;
                if($this->security->isGranted("ROLE_ENTERPRISE_ADMIN"))
                    return true;
                if($subject->getUsers()->count() == 1)
                    return true;
                break;
        }

        return false;
    }
}
