<?php

namespace App\Security\Voter;

use App\Entity\Mission;
use App\Service\Enumeration\StatusEnumeration;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class MissionVoter extends Voter
{
    public const UPDATE_MISSION = 'UPDATE_MISSION';

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::UPDATE_MISSION])
            && $subject instanceof \App\Entity\Mission;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::UPDATE_MISSION:
                if($subject->getStatus() == StatusEnumeration::COMPLETED)
                    return false;
                if ($subject->getIdAuthor() == $user)
                    return true;
                break;
        }

        return false;
    }

}
