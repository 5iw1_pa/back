<?php

namespace App\Security\Voter;

use App\Entity\Message;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class MessageVoter extends Voter
{
    public const UPDATE_MESSAGE = 'UPDATE_Message';

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::UPDATE_MESSAGE])
            && $subject instanceof \App\Entity\Message;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        // $subject = new Message();
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        if ($subject->getIdAuthor() == $user)
            return true;

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::UPDATE_MESSAGE:
                

                break;
        }

        return false;
    }

}

