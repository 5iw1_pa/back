<?php

namespace App\Security\Voter;

use App\Entity\Mission;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class TicketVoter extends Voter
{
    public const UPDATE_TICKET = 'UPDATE_Ticket';

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::UPDATE_TICKET])
            && $subject instanceof \App\Entity\Ticket;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        // $subject = new Ticket();
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        if ($subject->getIdAuthor() == $user)
            return true;

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::UPDATE_TICKET:

                break;
        }

        return false;
    }

}

