<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Ticket;
use App\Entity\User;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/message")
 */
class MessageController extends AbstractController
{
    /**
     * @Route("/", name="message_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(SerializerInterface $serializer, MessageRepository $messageRepository): Response
    {
        try
        {
            return new Response($serializer->serialize($messageRepository->findAll(), 'json',['groups' => 'get_message']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/ticket/{ticket}", name="message_create", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function create(SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager, Ticket $ticket): Response
    {
        try
        {
            $message = $serializer->deserialize($request->getContent(), Message::class, 'json',['groups' => 'create_message']);
            $errors = $validator->validate($message);
            if(count($errors))
                return new Response($serializer->serialize($errors, 'json'),400, ["content-type"=>"application/json"]);
            $message->setUserId($this->getUser());
            $message->setDateMessage(new \DateTime('NOW'));
            $message->setTicketId($ticket);
            $entityManager->persist($message);
            $entityManager->flush();
            return new Response($serializer->serialize($message, 'json',['groups' => 'get_message']),201, ["content-type"=>"application/json"]);
            
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="message_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function update(SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager, Message $message): Response
    {
        try
        {
            $message = $serializer->deserialize($request->getContent(), Message::class, 'json',['groups' => 'create_message', 'object_to_populate' => $message]);
            $errors = $validator->validate($message);
            if(count($errors))
                return new Response($serializer->serialize($errors, 'json'),400, ["content-type"=>"application/json"]);
            $entityManager->flush();
            return new Response($serializer->serialize($message, 'json',['groups' => 'get_message']),200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="message_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(EntityManagerInterface $entityManager,SerializerInterface $serializer, Message $message): Response
    {
        try
        {
            $entityManager->remove($message);
            $entityManager->flush();
            return new Response(null, 204, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }
}
