<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Profile;
use App\Repository\UserRepository;
use App\Service\MailerService;
use App\Service\Protocol\AmqpProtocol;
use App\Service\StatsServices;
use App\Service\TokenService;
use App\Service\UploadService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\ByteString;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/account")
 */
class AccountController extends AbstractController
{
    /**
     * @Route("/", name="account", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function account(SerializerInterface $serializer, UploadService $uploadService): Response
    {
        try
        {
            $user = $this->getUser();
            $fileName = $user->getPicture();
            if ($fileName !== null && count($fileName) > 0) {
                $picture = $uploadService->get($fileName[0]);
                if ($picture !== null) $user->setPicture([["name" => $fileName[0], "value" => $picture]]);
            }
            return new Response($serializer->serialize($user, "json", ["groups" => ["get_user","get_role"]]),200,["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }

    }

    /**
     * @Route("/", name="account_delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     */
    public function delete(EntityManagerInterface $entityManager,SerializerInterface $serializer,UserRepository $userRepository, AmqpProtocol $amqpProtocol): Response
    {
        try
        {
            $profile = $this->getUser()->getProfile();
            if ($this->isGranted('ROLE_ENTERPRISE_ADMIN') && $profile->getUsers()->count() > 1)
            {
                $nbAdmin = 0;
                foreach ($profile->getUsers() as $user)
                {
                    if (in_array("ROLE_ENTERPRISE_ADMIN",$user->getRoles()))
                        $nbAdmin ++;
                }
                if($nbAdmin == 1)
                    return new Response($serializer->serialize("Vous ne pouvez pas supprimer votre compte car vous etes le dernier admin de votre profile", 'json'),400, ["content-type"=>"application/json"]);
            }
            foreach ($this->getUser()->getMissionsParticipated() as $mission)
            {
                $mission->setIdCandidate(null);
                $entityManager->flush();
            }
            foreach ($this->getUser()->getMissionsCreated() as $mission)
            {
                $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
                $amqpProtocol->send("mission:delete", "darkmalt.exchange:mission.delete", $msg);
            }
            $msg = $serializer->serialize($this->getUser(), 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:delete", "darkmalt.exchange:user.delete", $msg);

            $entityManager->remove($this->getUser());

            if($profile->getUsers()->count() == 1)
                $entityManager->remove($profile);
            $entityManager->flush();

            return new Response(null, 204, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/", name="update_user", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     */
    public function updateUser(SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager, UploadService $uploadService, AmqpProtocol $amqpProtocol): Response 
    {
        try {
            $user = $this->getUser();
            $user = $serializer->deserialize($request->getContent(), User::class, 'json',['groups' => 'update_user', 'object_to_populate' => $user]);
            $errors = $validator->validate($user, null, 'update_user');
            if(count($errors))
                return new Response($serializer->serialize($errors, 'json'),400, ["content-type"=>"application/json"]);

            $pictures = $user->getPicture();
            $newPicture = $uploadService->checkUpload($pictures);
            $user->setPicture($newPicture);
            $entityManager->flush();

            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);

            return new Response($serializer->serialize($user, 'json',['groups' => 'get_user']), 200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);

        }
    }
    /**
     * @Route("/stats", name="stats_account", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function stats(SerializerInterface $serializer, StatsServices $statsServices)
    {
        try {
            $stats = [];
            if ($this->isGranted("ROLE_FREELANCE"))
                $stats = array_merge($stats, ["freelance" => $statsServices->getFreelanceStats($this->getUser())]);
            if ($this->isGranted("ROLE_ENTERPRISE"))
                $stats = array_merge($stats, ["enterprise" => $statsServices->getEnterpriseStats($this->getUser())]);
            if ($this->isGranted("ROLE_ENTERPRISE_ADMIN"))
                $stats = array_merge($stats, ["enterprise_admin" => $statsServices->getEnterpriseAdminStats($this->getUser())]);
            if ($this->isGranted("ROLE_ADMIN"))
                $stats = array_merge($stats, ["admin" => $statsServices->getAdminStats()]);

            return new Response($serializer->serialize($stats, 'json'), 200, ["content-type" => "application/json"]);
        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);

        }
    }

    /**
     * @Route("/confirme-email", name="confirmEmail_account", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     */
    public function confirmEmail(Request $request, MailerService $mailerService, EntityManagerInterface $entityManagers,SerializerInterface $serializer) : Response
    {
        $url = $request->query->get('url');
        if(empty($url))
            return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);

        try {
            $user = $this->getUser();
            if ($this->isGranted('ROLE_VALIDE') || $user == null) {
                return new Response($serializer->serialize("email deja valide", 'json'),400, ["content-type"=>"application/json"]);
            }
            $user->setToken(ByteString::fromRandom(32)->toString());
            $user->setDateToken(new \DateTime('NOW'));
            $entityManagers->flush();
            $mailerService->sendConfirmedMail($user, $url);
            return new Response($serializer->serialize("email envoye", 'json'),200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }


    }

    /**
     * @Route("/profile", name="account_profile", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function profile(SerializerInterface $serializer): Response
    {
        try
        {
            return new Response($serializer->serialize($this->getUser()->getProfile(), 'json',['groups' => ['get_profile','get_role','get_documents']]),200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile", name="update_profile", methods={"PUT"})
     */
    public function updateProfile(SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager, UploadService $uploadService, AmqpProtocol $amqpProtocol): Response
    {
        $profile = $this->getUser()->getProfile();
        $this->denyAccessUnlessGranted("UPDATE_PROFILE", $profile);

        $profile = $serializer->deserialize($request->getContent(), Profile::class, 'json',['groups' => 'update_profile', 'object_to_populate' => $profile]);
        $errors = $validator->validate($profile, null, "update_profile");
        if(count($errors))
            return new Response($serializer->serialize($errors, 'json'), 400, ["content-type"=>"application/json"]);

        $documents = $profile->getDocuments();
        $profileDocuments = $uploadService->checkUpload($documents);
        $profile->setDocuments($profileDocuments);
        
        $entityManager->flush();

        foreach ($profile->getUsers() as $user)
        {
            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);
        }

        return new Response($serializer->serialize($profile, 'json', ['groups' => ['get_profile','get_role','get_documents']]), 200, ["content-type"=>"application/json"]);
    }

    /**
     * @Route("/profile/add-admin/{id}", name="account_addAdmin", methods={"PUT"})
     * @IsGranted("ROLE_ENTERPRISE_ADMIN")
     */
    public function AddAdmin(SerializerInterface $serializer, User $user, EntityManagerInterface $entityManager): Response
    {
        if(!$this->getUser()->getProfile()->getUsers()->contains($user))
            return new Response($serializer->serialize("Cette utilisateur ne fait pas partie de votre entreprise", 'json'),400, ["content-type"=>"application/json"]);

        try
        {
            $user->addRoles("ROLE_ENTERPRISE_ADMIN");
            $user->removeRoles("ROLE_ENTERPRISE");
            $entityManager->flush();
            return new Response($serializer->serialize($this->getUser()->getProfile(), 'json', ['groups' => ['get_profile','get_role']]), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile/remove-user/{id}", name="account_removeUserProfile", methods={"DELETE"})
     * @IsGranted("ROLE_ENTERPRISE_ADMIN")
     */
    public function removeUserProfile(SerializerInterface $serializer, User $user, EntityManagerInterface $entityManager, AmqpProtocol $amqpProtocol): Response
    {
        if(!$this->getUser()->getProfile()->getUsers()->contains($user))
            return new Response($serializer->serialize("Cette utilisateur ne fait pas partie de votre entreprise", 'json'),400, ["content-type"=>"application/json"]);

        if(array_search("ROLE_ENTERPRISE_USER",$user->getRoles()))
            return new Response($serializer->serialize("Cette utilisateur est lui aussi admin", 'json'),400, ["content-type"=>"application/json"]);

        try
        {
            foreach ($user->getMissionsCreated() as $mission)
            {
                $user->removeMissionsCreated($mission);
                $this->getUser()->addMissionsCreated($mission);
            }
            $entityManager->flush();
            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:delete", "darkmalt.exchange:user.delete", $msg);
            $entityManager->remove($user);
            $entityManager->flush();
            return new Response(null, 204, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/change-email", name="account_changeEmail", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     */
    public function changeEmail(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, MailerService $mailerService, ValidatorInterface $validator): Response
    {
        $url = $request->query->get('url');
        $newEmail = $request->query->get('newEmail');
        if(empty($url))
            return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);
        if(empty($newEmail))
            return new Response($serializer->serialize("aucun nouveau email", 'json'), 400, ["content-type"=>"application/json"]);

        try
        {
            $checkEmail = new User();
            $checkEmail->setEmail($newEmail);
            $errors = $validator->validate($checkEmail, null, 'update_email');
            if (count($errors))
                return new Response($serializer->serialize($errors, 'json'), 400, ["content-type"=>"application/json"]);

            $user = $this->getUser();
            $user->setToken(ByteString::fromRandom(32)->toString());
            $user->setDateToken(new \DateTime('NOW'));

            $mailerService->sendChangeEmail($user, $url,$newEmail);

            $entityManager->flush();
            return new Response($serializer->serialize("Mail envoye", 'json'), 200, ["content-type"=>"application/json"]);

        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/new-email", name="account_newEmail", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     */
    public function newEmail(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, MailerService $mailerService, AmqpProtocol $amqpProtocol, UserRepository $userRepository, TokenService $tokenService): Response
    {
        $url = $request->query->get('url');
        $newEmail = $request->query->get('newEmail');
        if(empty($url))
            return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);
        if(empty($newEmail))
            return new Response($serializer->serialize("aucun nouveau email", 'json'), 400, ["content-type"=>"application/json"]);

        try
        {
            $user = $userRepository->findOneBy(['email'=>$request->query->get('email')]);
            $token = $request->query->get('token');
            $response = $tokenService->checkToken($user, $token);
            if($response->getStatusCode() != 200)
                return $response;

            $user = $this->getUser();
            $user->setEmail($newEmail);
            $user->setToken(ByteString::fromRandom(32)->toString());
            $user->setDateToken(new \DateTime('NOW'));
            $user->removeRoles("ROLE_VALIDE");

            $mailerService->sendConfirmedMail($user, $url);

            $entityManager->flush();
            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);
            return new Response($serializer->serialize("Email change et mail envoye", 'json'), 200, ["content-type"=>"application/json"]);

        }
        catch (\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }
}


