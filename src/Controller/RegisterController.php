<?php

namespace App\Controller;
use App\Entity\Profile;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\MailerService;
use App\Service\Protocol\AmqpProtocol;
use App\Service\TokenService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\ByteString;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager, ValidatorInterface $validator, SerializerInterface $serializer, AmqpProtocol $amqpProtocol, MailerService $mailerService): Response
    {
        $url = $request->query->get('url');
        if(empty($url))
            return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);

        try {
            $user = $serializer->deserialize($request->getContent(), User::class, 'json', ['groups' => 'create_user']);
            $profile = $serializer->deserialize($request->getContent(), Profile::class, 'json', ['groups' => 'create_user']);

            $errors = $validator->validate($user, null, ['add_user','password_user']);
            $profileError = $validator->validate($profile);
            $errors->addAll($profileError);

            if (count($errors))
                return new Response($serializer->serialize($errors, 'json'), 400,["content-type"=>"application/json"]);

            $profile->setCertified(false);
            $entityManager->persist($profile);

            $user->setPassword($passwordHasher->hashPassword($user, $user->getPassword()));
            $user->setProfile($profile);

            $user->setToken(ByteString::fromRandom(32)->toString());
            $user->setDateToken(new \DateTime('NOW'));
            $mailerService->sendRegisterConfirmedMail($user, $url);

            $entityManager->persist($user);
            $entityManager->flush();

            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:create", "darkmalt.exchange:user.create", $msg);

            return new Response($serializer->serialize($user, 'json', ['groups' => 'get_user']), 201,["content-type"=>"application/json"]);
        } catch (\Throwable $exception) {
            return new Response($serializer->serialize($exception, 'json'), 500,["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/confirmation-email", name="confirmedEmail_register", methods={"PUT"})
     */
    public function confirmedEmail(Request $request, UserRepository $userRepository, AccessDecisionManagerInterface $accessDecisionManager, EntityManagerInterface $entityManager, SerializerInterface $serializer, TokenService $tokenService) : Response
    {
        $user = $userRepository->findOneBy(['email'=>$request->query->get('email')]);
        $tokenUser = $request->query->get('token');
        $response = $tokenService->checkToken($user, $tokenUser);

        if($response->getStatusCode() != 200)
            return $response;

        $token = new UsernamePasswordToken($user, 'none', 'none',$user->getRoles());
        if($accessDecisionManager->decide($token,['ROLE_VALIDE']))
        {
            return new Response($serializer->serialize("l email à deja été valide", 'json'), 400, ["content-type"=>"application/json"]);
        }


        $user->addRoles('ROLE_VALIDE');
        $user->setToken(null);
        $user->setDateToken(null);
        $entityManager->flush();

        return new Response($serializer->serialize("l email a ete valide avec succes", 'json'), 200, ["content-type"=>"application/json"]);
    }

    /**
     * @Route("/add-user", name="addUser_register", methods={"POST"})
     * @IsGranted("ROLE_ENTERPRISE_ADMIN")
     */
    public function addUser(Request $request, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager, ValidatorInterface $validator, SerializerInterface $serializer, AmqpProtocol $amqpProtocol, MailerService $mailerService) : Response
    {
        if(empty($this->getUser()->getProfile()->getPremiumUntil()) || $this->getUser()->getProfile()->getPremiumUntil() < new \DateTime("NOW"))
            return new Response($serializer->serialize("Vous n'etes pas premium", 'json'), 403, ["content-type"=>"application/json"]);

        $url = $request->query->get('url');
        if(empty($url))
            return new Response($serializer->serialize("Aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);

        try {
            $user = $serializer->deserialize($request->getContent(), User::class, 'json', ['groups' => 'add_user']);

            $errors = $validator->validate($user,null,'add_user');
            if (count($errors))
                return new Response($serializer->serialize($errors, 'json'), 400, ["content-type"=>"application/json"]);

            $user->setPassword($passwordHasher->hashPassword($user, ByteString::fromRandom(32)->toString()));
            $user->addRoles("ROLE_ENTERPRISE");

            $user->setToken(ByteString::fromRandom(32)->toString());
            $user->setDateToken(new \DateTime('NOW'));

            $entityManager->persist($user);
            $this->getUser()->getProfile()->addUser($user);
            $mailerService->sendAddUserMail($user, $url);

            $entityManager->flush();

            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:create", "darkmalt.exchange:user.create", $msg);

            return new Response($serializer->serialize($user, 'json', ['groups' => 'get_user']), 201, ["content-type"=>"application/json"]);
        } catch (\Throwable $exception) {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/forgot-password", name="forgotPassword_register", methods={"POST"})
     */
    public function forgotPassword(Request $request, UserRepository $userRepository, MailerService $mailerService, EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        if ($this->getUser()) {
            return new Response($serializer->serialize("Action impossible en etant connecte", 'json'), 400, ["content-type"=>"application/json"]);
        }
        $url = $request->query->get('url');
            if(empty($url))
                return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);

        try{
            $email = $serializer->deserialize($request->getContent(), User::class, 'json', ['groups' => 'email_user']);
            $user = $userRepository->findOneBy(["email"=>$email->getEmail()]);

            if(empty($user))
                return new Response($serializer->serialize("email non trouve", 'json'), 400, ["content-type"=>"application/json"]);

            $user->setToken(ByteString::fromRandom(32)->toString());
            $user->setDateToken(new \DateTime('NOW'));
            $entityManager->flush();
            $mailerService->sendForgotPasswordMail($user,$url);
            return new Response($serializer->serialize("mail envoye", 'json'), 200, ["content-type"=>"application/json"]);
        }
        catch(\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/new-password", name="newPassword_register", methods={"POST"})
     */
    public function newPassword(Request $request, UserRepository $userRepository, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator, TokenService $tokenService)
    {
        $user = $userRepository->findOneBy(['email'=>$request->query->get('email')]);
        $token = $request->query->get('token');
        $response = $tokenService->checkToken($user, $token);

        if($response->getStatusCode() != 200)
            return $response;

        try {
            $password = $serializer->deserialize($request->getContent(), User::class, 'json', ['groups' => 'password_user']);
            $errors = $validator->validate($password, null, 'password_user');
            if (count($errors))
                return new Response($serializer->serialize($errors, 'json'), 400, ["content-type"=>"application/json"]);

            $user->setPassword($passwordHasher->hashPassword($user,$password->getPassword()));
            $user->addRoles("ROLE_VALIDE");
            $user->setToken(null);
            $user->setDateToken(null);

            $entityManager->flush();
            return new Response($serializer->serialize($user, 'json', ['groups' => 'get_user']), 200, ["content-type"=>"application/json"]);

        }
        catch(\Exception $exception)
        {
            return new Response($serializer->serialize($exception, 'json'), 500, ["content-type"=>"application/json"]);
        }
    }
}
