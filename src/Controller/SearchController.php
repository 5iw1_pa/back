<?php

namespace App\Controller;

use App\Service\Protocol\AmqpProtocol;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Service\Protocol\HttpProtocol;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\UploadService;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/search")
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/mission", name="find_all_missions", methods={"GET"})
     * @IsGranted("ROLE_CERTIFIED")
     */
    public function findAllMissions(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client): Response
    {
        return $httpProtocol->get("http://search:8080/mission", $client, $request->query, null);
    }

    /**
     * @Route("/mission/{id}", name="find_one_mission", methods={"GET"})
     * @IsGranted("ROLE_CERTIFIED")
     */
    public function findOneMission(HttpProtocol $httpProtocol, Request $request, HttpClientInterface $client): Response
    {
        return $httpProtocol->get("http://search:8080/mission/" . $request->attributes->get("id"), $client, null, null);
    }

    /**
     * @Route("/user", name="find_all_users", methods={"GET"})
     * @IsGranted("ROLE_CERTIFIED")
     */
    public function findAllUsers(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client): Response {
        return $httpProtocol->get("http://search:8080/user", $client, $request->query, null);
    }

    /**
     * @Route("/user/{id}", name="find_one_user", methods={"GET"})
     * @IsGranted("ROLE_CERTIFIED")
     */
    public function findOneUser(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client): Response {
        return $httpProtocol->get("http://search:8080/user/" . $request->attributes->get("id"), $client, null, null);
    }

    /**
     * @Route("/doc", name="find_document_by_name", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function findOneDocumentByName(Request $request, SerializerInterface $serializer, UploadService $uploadService): Response {
        try {
            $query = $request->query->get("n");
            if ($query === null)
                return new Response("Query \"n\" is missing!", 400, ["content-type"=>"application/json"]);
            $document = $uploadService->get($query);
            return new Response($serializer->serialize($document, "json"),200, ["content-type"=>"application/json"]);
        }
        catch (\Exception $e) {
            return new Response($e, 500, ["content-type"=>"application/json"]);
        }
    }
}
