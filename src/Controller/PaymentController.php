<?php

namespace App\Controller;

use App\Repository\ProfileRepository;
use App\Repository\UserRepository;
use App\Service\Protocol\AmqpProtocol;
use App\Service\Protocol\HttpProtocol;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
    /**
     * @Route("/", name="create_payment", methods={"POST"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function create(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, EntityManagerInterface $entityManager, Security $security, AmqpProtocol $amqpProtocol, SerializerInterface $serializer): Response
    {
        $payment = $httpProtocol->post("http://payment_api:80/payment/", $client, $request->getContent(), $security->getToken()->getCredentials());
        if($payment->getStatusCode() == 201 && json_decode($payment->getContent(),true)["status"] == 'succeeded')
        {
            $profile = $this->getUser()->getProfile();
            if($profile->getPremiumUntil() != null)
                $profile->setPremiumTry(true);
            $profile->setPremiumUntil(new \DateTime("+1 month"));

            $entityManager->flush();

            foreach ($profile->getUsers() as $user)
            {
                $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
                $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);
                foreach ($user->getMissionsCreated() as $mission)
                {
                    $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
                    $amqpProtocol->send("mission:update", "darkmalt.exchange:mission.update", $msg);
                }

            }

        }

        return $payment;
    }

    /**
     * @Route("/", name="get_all_payment", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getAll(HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/payment/", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/no-succeeded", name="payment_allNotSucceeded", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function allNotSucceeded(HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/payment/no-succeeded", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/user", name="payment_getByUser", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function getByUser(HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/payment/user", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/user/{user_id}", name="get_payment_for_one_user", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getForOneUser(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, UserRepository $userRepository, Security $security): Response
    {
        $user_id = $request->attributes->get("user_id");
        $user = $userRepository->findOneBy(["id" => (int)$user_id]);
        if ($user === null)
            return new Response("User not found!", 404, ["content-type"=>"application/json"]);
        return $httpProtocol->get("http://payment_api:80/payment/user/${user_id}", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/profile", name="payment_getByProfile", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function getByProfile(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, UserRepository $userRepository, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/payment/profile", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/profile/{profile_id}", name="get_payment_for_one_user", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getForOneProfile(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, UserRepository $userRepository, Security $security): Response
    {
        $user_id = $request->attributes->get("user_id");
        $user = $userRepository->findOneBy(["id" => (int)$user_id]);
        if ($user === null)
            return new Response("User not found!", 404, ["content-type"=>"application/json"]);
        return $httpProtocol->get("http://payment_api:80/payment/user/${user_id}", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/user", name="payment_getRefundsByUser", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function getRefundsByUser(HttpProtocol $httpProtocol, HttpClientInterface $client, UserRepository $userRepository, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/refund/user", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/user/{user_id}", name="payment_getRefundsForOneUser", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getRefundsForOneUser(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, UserRepository $userRepository, Security $security): Response
    {
        $user_id = $request->attributes->get("user_id");
        $user = $userRepository->findOneBy(["id" => (int)$user_id]);
        if ($user === null)
            return new Response("User not found!", 404, ["content-type"=>"application/json"]);
        return $httpProtocol->get("http://payment_api:80/refund/user/${user_id}", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/profile", name="payment_getRefundsByProfile", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function getRefundsByProfile(HttpProtocol $httpProtocol, HttpClientInterface $client, UserRepository $userRepository, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/refund/profile", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/profile/{profile_id}", name="payment_getRefundsForOneProfile", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getRefundsForOneProfile(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, UserRepository $userRepository, Security $security): Response
    {
        $profile_id = $request->attributes->get("profile_id");
        $user = $userRepository->findOneBy(["id" => (int)$profile_id]);
        if ($user === null)
            return new Response("User not found!", 404, ["content-type"=>"application/json"]);
        return $httpProtocol->get("http://payment_api:80/refund/profile/${$profile_id}", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund", name="get_all_refund", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getAllRefunds(HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/refund/", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/create/{id}", name="create_refund", methods={"POST"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function createRefund(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        if($this->getUser()->getProfile()->getPremiumTry())
            return new Response("Essaie gratuit deja utilise", 400, ["content-type"=>"application/json"]);
        $id = $request->attributes->get("id");
        return $httpProtocol->post("http://payment_api:80/refund/create/${id}", $client, $request->getContent(), $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/refunded/{id}", name="payment_refundedRefund", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function refundedRefund(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security, ProfileRepository $profileRepository, EntityManagerInterface $entityManager, AmqpProtocol $amqpProtocol, SerializerInterface $serializer): Response
    {
        $id = $request->attributes->get("id");
        $response = $httpProtocol->put("http://payment_api:80/refund/refunded/${id}", $client, $request->getContent(), $security->getToken()->getCredentials());
        if($response->getStatusCode() == 200)
        {
            $refund = json_decode($response->getContent(),true);
            $profile = $profileRepository->find($refund['profile_id']);
            $profile->setPremiumTry(true);
            $profile->setPremiumUntil(null);

            $entityManager->flush();

            foreach ($profile->getUsers() as $user)
            {
                $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
                $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);
                foreach ($user->getMissionsCreated() as $mission)
                {
                    $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
                    $amqpProtocol->send("mission:update", "darkmalt.exchange:mission.update", $msg);
                }
            }
        }
        return $response;
    }

    /**
     * @Route("/refund/refused/{id}", name="payment_refusedRefund", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function refusedRefund(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        $id = $request->attributes->get("id");
        return $httpProtocol->put("http://payment_api:80/refund/refused/${id}", $client, $request->getContent(), $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/cancel/{id}", name="payment_cancelRefund", methods={"PUT"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function cancelRefund(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        $id = $request->attributes->get("id");
        return $httpProtocol->put("http://payment_api:80/refund/cancel/${id}", $client, $request->getContent(), $security->getToken()->getCredentials());
    }

    /**
     * @Route("/refund/{id}", name="get_one_refund", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function getOneRefund(Request $request, HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        $id = $request->attributes->get("id");
        return $httpProtocol->get("http://payment_api:80/refund/${id}", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/create-checkout-session", name="stripe_checkout_session", methods={"POST"})
     */
    public function createStripeCheckoutSession(HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->post("http://payment_api:80/payment/create-checkout-session", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/offer", name="payment_getOffer", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function getOffer(Request $request,HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->get("http://payment_api:80/offer/", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/offer/enterprise", name="payment_updateEnterpriseOffer", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateEnterpriseOffer(Request $request,HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->put("http://payment_api:80/offer/enterprise", $client, $request->getContent(), $security->getToken()->getCredentials());
    }

    /**
     * @Route("/offer/freelance", name="payment_updateFreelanceOffer", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateFreelanceOffer(Request $request,HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        return $httpProtocol->put("http://payment_api:80/offer/freelance", $client, $request->getContent(), $security->getToken()->getCredentials());
    }

    /**
     * @Route("/{id}", name="payment_getOnePayment", methods={"GET"})
     * @IsGranted("ROLE_GESTION_PROFILE")
     */
    public function getOnePayment(Request $request,HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security): Response
    {
        $id = $request->attributes->get("id");
        return $httpProtocol->get("http://payment_api:80/payment/${id}", $client, null, $security->getToken()->getCredentials());
    }

    /**
     * @Route("/{id}", name="payment_UpdateStatus", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function UpdateStatus(Request $request,HttpProtocol $httpProtocol, HttpClientInterface $client, Security $security, EntityManagerInterface $entityManager, AmqpProtocol $amqpProtocol, SerializerInterface $serializer, ProfileRepository $profileRepository): Response
    {
        $id = $request->attributes->get("id");
        $response = $httpProtocol->Put("http://payment_api:80/payment/${id}", $client, null, $security->getToken()->getCredentials());
        $payment = json_decode($response->getContent(),true);
        if($response->getStatusCode() == 200 && $payment["status"] == 'succeeded')
        {
            $profile = $profileRepository->find($payment['profile_id']);
            if($profile->getPremiumUntil() != null)
                $profile->setPremiumTry(true);

            $profile->setPremiumUntil(new \DateTime("+1 month"));

            $entityManager->flush();

            foreach ($profile->getUsers() as $user)
            {
                $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
                $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);
            }
        }

        return $response;
    }


}
