<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/users")
 */
class UsersController extends AbstractController
{
    /**
     * @Route("/", name="users_index", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function index(SerializerInterface $serializer, UserRepository $userRepository): Response
    {
        try
        {
            return new Response($serializer->serialize($userRepository->findAll(), 'json',['groups' => 'get_user']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="users_GetOneUser", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getOneUser(SerializerInterface $serializer, User $user): Response
    {
        try
        {
            return new Response($serializer->serialize( $user, 'json',['groups' => 'get_user']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile", name="users_profiles", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function profiles(SerializerInterface $serializer, UserRepository $userRepository): Response
    {
        try
        {
            return new Response($serializer->serialize($userRepository->findAll(), 'json',['groups' => 'get_profile']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile/{id}", name="users_GetProfile", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function GetProfile(SerializerInterface $serializer, Profile $profile): Response
    {
        try
        {
            return new Response($serializer->serialize( $profile, 'json',['groups' => 'get_profile']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }
}
