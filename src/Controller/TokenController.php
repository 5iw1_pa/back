<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\TokenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class TokenController extends AbstractController
{
    /**
     * @Route("/check-token", name="check_token")
     */
    public function check(Request $request,TokenService $tokenService, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['email'=>$request->query->get('email')]);
        $token = $request->query->get('token');
        return $tokenService->checkToken($user, $token);
    }
}
