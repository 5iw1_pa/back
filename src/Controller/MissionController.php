<?php

namespace App\Controller;

use App\Entity\Mission;
use App\Entity\User;
use App\Repository\MissionRepository;
use App\Service\Enumeration\StatusEnumeration;
use App\Service\MailerService;
use App\Service\Protocol\AmqpProtocol;
use App\Service\UploadService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/mission")
 */
class MissionController extends AbstractController
{
    /**
     * @Route("/", name="mission_index", methods={"GET"})
     * @IsGranted("ROLE_CERTIFIED")
     */
    public function index(SerializerInterface $serializer, MissionRepository $missionRepository): Response
    {
        try
        {
            return new Response($serializer->serialize($missionRepository->findAll(), 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/", name="mission_create", methods={"POST"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function create(SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager, UploadService $uploadService, AmqpProtocol $amqpProtocol): Response
    {
        try
        {              
            $mission = $serializer->deserialize($request->getContent(), Mission::class, 'json',['groups' => 'create_mission']);
            $errors = $validator->validate($mission);
            if(count($errors))
                return new Response($serializer->serialize($errors, 'json'),400, ["content-type"=>"application/json"]);
            $mission->setIdAuthor($this->getUser());
            $mission->setPublicationDate(new \DateTime('NOW'));
            $mission->setStatus(StatusEnumeration::COMING);

            $documents = $mission->getDocuments();
            $missionDocuments = $uploadService->checkUpload($documents);
            $mission->setDocuments($missionDocuments);

            $entityManager->persist($mission);
            $entityManager->flush();

            $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
            $amqpProtocol->send("mission:create", "darkmalt.exchange:mission.create", $msg);

            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']),201, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/author-missions", name="mission_authorMission", methods={"GET"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function authorMission(SerializerInterface $serializer): Response
    {
        try
        {
            return new Response($serializer->serialize($this->getUser()->getMissionsCreated(), 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/participe-missions", name="mission_participeMission", methods={"GET"})
     * @IsGranted("ROLE_FREELANCE")
     */
    public function participeMission(SerializerInterface $serializer): Response
    {
        try
        {
            return new Response($serializer->serialize($this->getUser()->getMissionsParticipated(), 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/candidated-missions", name="mission_candidatedMission", methods={"GET"})
     * @IsGranted("ROLE_FREELANCE")
     */
    public function candidatedMission(SerializerInterface $serializer): Response
    {
        try
        {
            return new Response($serializer->serialize($this->getUser()->getCandidateMission(), 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/apply/{id}", name="mission_apply", methods={"PUT"})
     * @IsGranted("ROLE_FREELANCE")
     */
    public function apply(SerializerInterface $serializer, Mission $mission, EntityManagerInterface $entityManager): Response
    {
        try
        {
            $mission->addCandidate($this->getUser());
            $entityManager->flush();
            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/remove-apply/{id}", name="mission_removeApply", methods={"PUT"})
     * @IsGranted("ROLE_FREELANCE")
     */
    public function removeApply(SerializerInterface $serializer, Mission $mission, EntityManagerInterface $entityManager): Response
    {
        try
        {
            $mission->removeCandidate($this->getUser());
            $entityManager->flush();
            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }
    /**
     * @Route("/notify/{user}/{mission}", name="mission_notifyUser", methods={"GET"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function notifyUser(Request $request, SerializerInterface $serializer,User $user ,Mission $mission, EntityManagerInterface $entityManager, MailerService $mailerService): Response
    {
        $this->denyAccessUnlessGranted("UPDATE_MISSION", $mission);
        $url = $request->query->get('url');
        if(empty($url))
            return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);

        if($mission->getCandidate()->contains($user) || $mission->getIdCandidate() === $user)
            return new Response($serializer->serialize("l'utilisateur est deja inscrit sur la mission", 'json'), 400, ["content-type"=>"application/json"]);

        try
        {
            $mailerService->sendNotifyUser($user,$mission,$url);
            return new Response($serializer->serialize("mail notification envoye", 'json'), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/choice/{user}/{mission}", name="mission_choiceUser", methods={"PUT"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function choiceUser(Request $request, SerializerInterface $serializer,User $user ,Mission $mission, EntityManagerInterface $entityManager, MailerService $mailerService, AmqpProtocol $amqpProtocol): Response
    {
        $this->denyAccessUnlessGranted("UPDATE_MISSION", $mission);
        if(!$mission->getCandidate()->contains($user))
            return new Response($serializer->serialize("L'utilisateur choisi n'as pas candidaté pour cette mission", 'json'),400, ["content-type"=>"application/json"]);

        $url = $request->query->get('url');
        if(empty($url))
            return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);

        try
        {
            $mission->setStatus(StatusEnumeration::IN_PROGRESS);
            $mission->removeCandidate($user);
            $mission->setIdCandidate($user);
            $entityManager->flush();
            $mailerService->sendChoiceUser($user,$mission,$url);

            $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
            $amqpProtocol->send("mission:update", "darkmalt.exchange:mission.update", $msg);

            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/remove-choice/{mission}", name="mission_removeChoiceUser", methods={"PUT"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function removeChoiceUser(SerializerInterface $serializer ,Mission $mission, EntityManagerInterface $entityManager, MailerService $mailerService, AmqpProtocol $amqpProtocol): Response
    {
        $this->denyAccessUnlessGranted("UPDATE_MISSION", $mission);
        try
        {
            $candidate = $mission->getIdCandidate();
            if(empty($candidate))
                return new Response($serializer->serialize("Personne n'est ne travail sur cette mission", 'json'),400, ["content-type"=>"application/json"]);

            $mission->setStatus(StatusEnumeration::COMING);
            $mission->setIdCandidate(null);
            $entityManager->flush();
            $mailerService->sendRemoveChoiceUser($candidate, $mission);
            
            $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
            $amqpProtocol->send("mission:update", "darkmalt.exchange:mission.update", $msg);

            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/abandon/{mission}", name="mission_abandon", methods={"PUT"})
     * @IsGranted("ROLE_FREELANCE")
     */
    public function abandon(Request $request, SerializerInterface $serializer ,Mission $mission, EntityManagerInterface $entityManager, MailerService $mailerService, AmqpProtocol $amqpProtocol): Response
    {
        if(!$this->getUser()->getMissionsParticipated()->contains($mission))
            return new Response($serializer->serialize("Utilisateur non choisi sur la mission", 'json'),400, ["content-type"=>"application/json"]);
        
        $url = $request->query->get('url');
        if(empty($url))
            return new Response($serializer->serialize("aucune url de retour", 'json'), 400, ["content-type"=>"application/json"]);

        try
        {
            $mission->setStatus(StatusEnumeration::COMING);
            $mission->setIdCandidate(null);
            $entityManager->flush();
            $mailerService->sendAbandonMission($mission,$url);

            $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
            $amqpProtocol->send("mission:update", "darkmalt.exchange:mission.update", $msg);

            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/completed/{id}", name="mission_completed", methods={"PUT"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function completed(SerializerInterface $serializer,EntityManagerInterface $entityManager, Mission $mission, AmqpProtocol $amqpProtocol): Response
    {
        $this->denyAccessUnlessGranted("UPDATE_MISSION", $mission);
        try
        {
            $mission->setStatus(StatusEnumeration::COMPLETED);
            $entityManager->flush();

            $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
            $amqpProtocol->send("mission:delete", "darkmalt.exchange:mission.delete", $msg);

            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']),200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="mission_update", methods={"PUT"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function update(SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager, Mission $mission, UploadService $uploadService, AmqpProtocol $amqpProtocol): Response
    {
        $this->denyAccessUnlessGranted("UPDATE_MISSION", $mission);
        try
        {
            $mission = $serializer->deserialize($request->getContent(), Mission::class, 'json',['groups' => 'create_mission', 'object_to_populate' => $mission]);
            $errors = $validator->validate($mission);
            if(count($errors))
                return new Response($serializer->serialize($errors, 'json'),400, ["content-type"=>"application/json"]);

            $documents = $mission->getDocuments();
            $missionDocuments = $uploadService->checkUpload($documents);
            $mission->setDocuments($missionDocuments);

            $entityManager->flush();

            $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
            $amqpProtocol->send("mission:update", "darkmalt.exchange:mission.update", $msg);

            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']),200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="mission_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ENTERPRISE")
     */
    public function delete(EntityManagerInterface $entityManager,SerializerInterface $serializer, Mission $mission, AmqpProtocol $amqpProtocol): Response
    {
        $this->denyAccessUnlessGranted("UPDATE_MISSION", $mission);
        try
        {
            $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
            $amqpProtocol->send("mission:delete", "darkmalt.exchange:mission.delete", $msg);
            
            $entityManager->remove($mission);
            $entityManager->flush();
            return new Response(null, 204, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="mission_getMission", methods={"GET"})
     */
    public function getMission(SerializerInterface $serializer, Mission $mission): Response
    {
        try
        {
            return new Response($serializer->serialize($mission, 'json',['groups' => 'get_mission']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

}
