<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Entity\User;
use App\Entity\Message;
use App\Repository\TicketRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\Enumeration\StatusEnumeration;

/**
 * @Route("/ticket")
 */
class TicketController extends AbstractController
{
    /**
     * @Route("/", name="ticket_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(SerializerInterface $serializer, TicketRepository $ticketRepository): Response
    {
        try
        {
            return new Response($serializer->serialize($ticketRepository->findAll(), 'json',['groups' => 'get_ticket']), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }


    /**
     * @Route("/", name="ticket_create", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function create(SerializerInterface $serializer, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager): Response
    {
        try
        {     
            $ticket = $serializer->deserialize($request->getContent(), Ticket::class, 'json',['groups' => 'create_ticket']);
            $message = $serializer->deserialize($request->getContent(), Message::class, 'json',['groups' => 'create_message']);
            $errors = $validator->validate($ticket);
            if(count($errors))
                return new Response($serializer->serialize($errors, 'json'),400, ["content-type"=>"application/json"]);
                
            $ticket->setUserId($this->getUser());
            $ticket->setDateTicket(new \DateTime('NOW'));
            $ticket->setStatus(StatusEnumeration::OPEN);

            $message->setUserId($this->getUser());
            $message->setDateMessage(new \DateTime('NOW'));
            $message->setTicketId($ticket->getId());
            $message->setDescription($ticket->getDescription());
            
            $ticket->addMessage($message);

            $entityManager->persist($ticket);
            $entityManager->persist($message);
            $entityManager->flush();

            return new Response($serializer->serialize($ticket, 'json',['groups' => 'get_ticket']),201, ["content-type"=>"application/json"]);
            

        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }


    /**
     * @Route("/{id}", name="ticket_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(EntityManagerInterface $entityManager,SerializerInterface $serializer, Ticket $ticket): Response
    {
        try
        {
            $entityManager->remove($ticket);
            $entityManager->flush();
            return new Response(null, 204, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/{id}", name="ticket_close", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function update(Request $request, SerializerInterface $serializer ,Ticket $ticket, EntityManagerInterface $entityManager): Response
    {
        try
        {
            $ticket->setStatus(StatusEnumeration::CLOSE);
            $entityManager->flush();
            return new Response($serializer->serialize($ticket, 'json',['groups' => 'get_ticket']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/own-ticket", name="ticket_getOwnTicket", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getOwnTicket(SerializerInterface $serializer): Response
    {
        try
        {
            return new Response($serializer->serialize($this->getUser()->getTickets(), 'json',['groups' => 'get_ticket']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {   
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }


    /**
     * @Route("/{id}", name="ticket_getTicket", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function getTicket(SerializerInterface $serializer, Ticket $ticket): Response
    {
        try
        {
            return new Response($serializer->serialize($ticket, 'json',['groups' => 'get_ticket']), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }


    
}