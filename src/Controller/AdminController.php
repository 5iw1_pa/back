<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Entity\User;
use App\Repository\ProfileRepository;
use App\Service\MailerService;
use App\Service\Protocol\AmqpProtocol;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/admin")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/profile/{id}/validate", name="admin_ValidateProfile", methods={"PUT"})
     */
    public function validateProfile(SerializerInterface $serializer, EntityManagerInterface $entityManager, Profile $profile, MailerService $mailerService, AmqpProtocol $amqpProtocol): Response
    {
        try
        {
            if($profile->getStatus() == 1)
                $profile->getUsers()->first()->addRoles("ROLE_ENTERPRISE_ADMIN");
            else
                $profile->getUsers()->first()->addRoles("ROLE_FREELANCE");
            $profile->setCertified(true);

            $mailerService->sendCertifiedMail($profile->getUsers()->first());
            $entityManager->flush();
            $users = $profile->getUsers();
            for ($i = 0; $i<count($users); $i++) {
                $msg = $serializer->serialize($users[$i], 'json', ['groups' => 'user_search_api']);
                $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);
            }
            return new Response($serializer->serialize( $profile, 'json',['groups' => ['get_profile','get_role','get_documents']]), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile-uncertified", name="admin_uncertifiedProfile", methods={"GET"})
     */
    public function uncertifiedProfile(SerializerInterface $serializer, ProfileRepository $profileRepository): Response
    {
        try
        {
            return new Response($serializer->serialize($profileRepository->findBy(['certified'=>false]), 'json',['groups' => ['get_profile','get_role','get_documents']]), 200, ["content-type"=>"application/json"]);
        }

        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/user/{id}", name="admin_deleteUser", methods={"DELETE"})
     */
    public function deleteUser(SerializerInterface $serializer, User $user, EntityManagerInterface $entityManager, AmqpProtocol $amqpProtocol): Response
    {
        try
        {
            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:delete", "darkmalt.exchange:user.delete", $msg);
            $entityManager->remove($user);
            if($user->getProfile()->getUsers()->count() == 1)
                $entityManager->remove($user->getProfile());

            foreach ($user->getMissionsParticipated() as $mission)
            {
                $mission->setIdCandidate(null);
                $entityManager->flush();
            }
            foreach ($user->getMissionsCreated() as $mission)
            {
                $msg = $serializer->serialize($mission, 'json', ['groups' => 'mission_search_api']);
                $amqpProtocol->send("mission:delete", "darkmalt.exchange:mission.delete", $msg);
            }

            $entityManager->flush();
            return new Response(null, 204, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/user/{id}", name="admin_getOneUser", methods={"GET"})
     */
    public function getOneUser(SerializerInterface $serializer, User $user): Response
    {
        try
        {
            return new Response($serializer->serialize($user, 'json',['groups' => ['get_user','get_role','get_documents']]), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/user/{id}", name="admin_updateUser", methods={"PUT"})
     */
    public function updateUser(SerializerInterface $serializer, User $user, Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager, AmqpProtocol $amqpProtocol): Response
    {
        try
        {
            $user = $serializer->deserialize($request->getContent(), User::class, 'json',['groups' => ['update_user_admin'], 'object_to_populate' => $user]);
            $errors = $validator->validate($user);
            if(count($errors))
                return new Response($serializer->serialize($errors, 'json'),400, ["content-type"=>"application/json"]);

            $entityManager->flush();
            $msg = $serializer->serialize($user, 'json', ['groups' => 'user_search_api']);
            $amqpProtocol->send("user:update", "darkmalt.exchange:user.update", $msg);
            return new Response($serializer->serialize($user, 'json',['groups' => ['get_user','get_role','get_documents']]), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

    /**
     * @Route("/profile/{id}", name="admin_getOneProfile", methods={"GET"})
     */
    public function getOneProfile(SerializerInterface $serializer, Profile $profile): Response
    {
        try
        {
            return new Response($serializer->serialize($profile, 'json',['groups' => ['get_profile','get_role','get_documents']]), 200, ["content-type"=>"application/json"]);
        }
        catch(\Throwable $exception)
        {
            return new Response($serializer->serialize($exception, 'json'),500, ["content-type"=>"application/json"]);
        }
    }

}
