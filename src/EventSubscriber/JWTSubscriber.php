<?php

namespace App\EventSubscriber;

use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class JWTSubscriber implements EventSubscriberInterface
{
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function onLexikJwtAuthenticationOnJwtCreated(JWTCreatedEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
        if($user->getProfile()->getUsers()->count() > 1)
        {
            if($user->getProfile()->getPremiumUntil() == null || $user->getProfile()->getPremiumUntil() < new \DateTime("NOW"))
            {
                $usersProfile = $this->userRepository->createQueryBuilder('u')
                    ->where('u.profile = :profile')
                    ->orderBy('u.id','ASC')
                    ->setParameter('profile',$user->getProfile()->getId())
                    ->getQuery()
                    ->getResult();

                $userEnterpriseAdmin = $user;
                foreach ($usersProfile as $userProfile)
                {
                    if(in_array("ROLE_ENTERPRISE_ADMIN",$userProfile->getRoles(),true))
                    {
                        $userEnterpriseAdmin = $userProfile;
                        break;
                    }

                }
                if($userEnterpriseAdmin != $user)
                {
                    $event->setData(["roles"=>["ROLE_DESACTIVATED"]]);
                    return;
                }
            }
        }
        $data["id_user"] = $user->getId();
        $data["id_profile"] = $user->getProfile()->getId();
        $event->setData($data);
    }

    public static function getSubscribedEvents()
    {
        return [
            'lexik_jwt_authentication.on_jwt_created' => 'onLexikJwtAuthenticationOnJwtCreated',
        ];
    }
}
